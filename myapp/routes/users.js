const express = require('express')
const router = express.Router()
const UserModel = require('../model/user')

router.get('/:id', async (req, res) => {
    try {
        const id = req.params.id
         const result = await UserModel.findById(id)
         res.status(200).send(result)    
    } catch (err) {
         res.status(500).json({error: err.message})
    }
})




module.exports = router