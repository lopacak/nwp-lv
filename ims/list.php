<?php
    include "header.php";
    include "connection.php";
$sql = "SELECT * FROM product ";
$result = mysqli_query($conn, $sql);

if (isset($_POST['list_asc'])) 
{
  $result = mysqli_query($conn, "SELECT * FROM product ORDER BY name ASC");
 // if($update_query){
   //  header('location:list.php');};

if (isset($_POST['list_desc'])) 
{
  $result = mysqli_query($conn, "SELECT * FROM product ORDER BY name DESC");
//  if($update_query){
  //   header('location:list.php');};
if (isset($_POST['unit_asc'])) 
{
  $result = mysqli_query($conn, "SELECT * FROM product ORDER BY unit ASC");
//  if($update_query){
  //   header('location:list.php');};
if (isset($_POST['unit_desc'])) 
{
  $result = mysqli_query($conn, "SELECT * FROM product ORDER BY unit DESC");
//  if($update_query){
  //   header('location:list.php');};

};
};
};
};

?>
<html>
<head>
    <title></title>

<style>
.dropbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
       <tr>
          <div class="dropdown">
            <h4>Sort List</h4>

          <div class="dropdown-content">
             <td><button type="submit" class="btn btn-primary" name="list_asc">Name ASC</button></td>
            <td><button type="submit" class="btn btn-primary" name="list_desc">Name DESC</button></td>
            <td><button type="submit" class="btn btn-primary" name="unit_asc">Unit# ASC</button></td>
            <td><button type="submit" class="btn btn-primary" name="unit_desc">Unit# DESC</button></td>
             </tr>
                </form>
</div>
</div>
    <table class="table table-striped">
  <thead>
    <tr>
      <!--<th scope="col">#</th>-->
      <th scope="col">Product Name</th>
      <th scope="col">Description</th>
      <th scope="col">Unit</th>
      <th scope="col">Location</th>
     
    </tr>
  </thead>
  <tbody>
   
      <?php
          if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
              ?>
             <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
               <tr>
               <input type="hidden" name="id"  value="<?php echo $row['id'];?>">
                <input type="hidden" name="name"  value="<?php echo $row['name'];?>">
                <input type="hidden" name="des"  value="<?php echo $row['des'];?>">
               <input type="hidden" name="unit"  value="<?php echo $row['unit'];?>">
                <input type="hidden" name="location"  value="<?php echo $row['location'];?>">
                <td><?php echo $row['name'];?></td>
                <td><?php echo $row['des'];?></td>
                <td><?php echo $row['unit'];?></td>
                <td><?php echo $row['location'];?></td>
                
                </tr>
                </form>
                <?php }
        } else {
            echo "0 results";
        }
        ?>
      

    
  </tbody>
</table>
</div>
</body>
</html>