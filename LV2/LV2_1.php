<?php


$conn = new mysqli("localhost", "root", "", "encDB");


if ($conn->connect_error) {
    die('Fail: ' . $conn->connect_error);
}

$tables = array();
$result = $conn->query('SHOW TABLES');
while ($row = $result->fetch_array()) {
    $tables[] = $row[0];
}

foreach ($tables as $table) {
    $filename = 'backups/' . $table . '_' . date('Y-d-m_H-i-s') . '.txt';
   
	$fp = fopen($filename,"wb");


    
    $result = $conn->query("SELECT * FROM $table");
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        $sql = "INSERT INTO $table VALUES (";
        for ($i = 0; $i < count($row); $i++) {
            $sql .= "'" . $conn->real_escape_string($row[$i]) . "',";
        }
        $sql = rtrim($sql, ',') . ");\n";
      
	fwrite($fp,$sql);
    }
	fclose($fp);
   
}

echo 'Backup success!';
