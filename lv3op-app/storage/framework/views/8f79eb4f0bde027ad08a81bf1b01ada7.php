<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2>My Projects</h2>
        <?php if(count($projects) > 0): ?>
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($project->name); ?></td>
                        <td><?php echo e($project->description); ?></td>
                        <td><?php echo e($project->start_date); ?></td>
                        <td><?php echo e($project->end_date); ?></td>
                        <td><?php echo e($project->price); ?></td>
                        <td>
                            <a href="<?php echo e(route('projects.edit', $project->id)); ?>" class="btn btn-primary">Edit</a>
                            <form action="<?php echo e(route('projects.destroy', $project->id)); ?>" method="POST" style="display: inline-block;">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('DELETE')); ?>

                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this project?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        <?php else: ?>
            <p>You don't have any projects yet.</p>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Luka\lv3op-app\resources\views/home.blade.php ENDPATH**/ ?>