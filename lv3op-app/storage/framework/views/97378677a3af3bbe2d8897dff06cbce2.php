<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><?php echo e(__('Project Details')); ?></div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Project Name')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->name); ?></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Project Description')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->description); ?></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Price')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->price); ?></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="members" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Project Members')); ?></label>
                            <div class="col-md-6">
                                <ul>
                                    <?php $__currentLoopData = $project->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($member->name); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="completed_tasks" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Project Completed Tasks')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->completed_tasks); ?></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="start_date" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Start Date')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->start_date); ?></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="end_date" class="col-md-4 col-form-label text-md-right"><?php echo e(__('End Date')); ?></label>

                            <div class="col-md-6">
                                <p><?php echo e($project->end_date); ?></p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="<?php echo e(route('projects.edit', $project)); ?>" class="btn btn-primary"><?php echo e(__('Edit Project')); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Luka\lv3op-app\resources\views/projects/show.blade.php ENDPATH**/ ?>